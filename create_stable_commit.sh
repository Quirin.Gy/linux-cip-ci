#!/bin/bash
#
# Copyright (C) 2020, Renesas Electronics Europe GmbH, Chris Paterson
# <chris.paterson2@renesas.com>
#
# This script gets the latest commit from the provided logfile and creates a
# corresponding commit to be added to the linux-stable-rc-ci repository in order
# to trigger a CI build.
#
# Script specific dependencies:
# grep cut sed git
#
# Paremeters
# $1: File to gather commit information from
#
################################################################################
COMMIT=""
AUTHOR=""
SUMMARY=""

if [ ! -f "${1}" ]; then
	echo """${1}"" does not exist"
	exit 1
fi

COMMIT=$(grep "commit" "${1}" | cut -c 8-19)
AUTHOR=$(grep "Author" "${1}" | cut -c 9-)
SUMMARY=$(sed -n '5 p' < "${1}" | cut -c 5-)

echo "Creating commit:"
echo "  Author: ${AUTHOR}"
echo "  Summary: ${SUMMARY} (${COMMIT})"

git add "${1}" || exit 1
git commit --author="${AUTHOR}" -m "${SUMMARY} (${COMMIT})" || exit 1
